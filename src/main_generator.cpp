#include "grid_square.h"
#include "gen_dfs.h"
#include "gen_dfs_imperfect.h"
#include "gen_prims.h"
#include <iostream>
#include <stdlib.h>
#include <time.h>

#include <sstream>
#include <string.h>

enum ChoiceGrid {
    CG_SQUARE
};

enum ChoiceGenerator {
    CG_DFS,
    CG_DFS_IMPERFECT,
    CG_PRIMS,
    CG_RANDOM
};

int main(int argc, char** argv) {
    const char * help_text = (
        "Usage: `generator [-a algorithm] [-s shape] <width> <height>`\n\n"
        "  Where \"<width>,<height>\" represents the arguments to a given grid's constructor.\n\n"
        "-a algorithm    The algorithm with which to generate the maze.\n"
        "                The default option is \"random\", which will select a random algorithm from the other available options.\n" 
        "                Options: dfs, dfsimperfect, prims, random\n\n"
        "-g grid         Specifies the grid with which the maze will be built upon.\n"
        "                The grid informs the arguments which are required in the final argument.\n"
        "                The default option is \"square\".\n"
        "                Options: square (width height)\n\n"
        "-h/--help       Print this help text.\n"
    );

    auto c_grid = CG_SQUARE;
    auto c_gen  = CG_RANDOM;

    if (![&]()->bool {
        for (int i = 1; i < argc; ++i) {
            if (strcmp(argv[i], "-h") == 0 || strcmp(argv[1], "--help") == 0) {
                std::cout << help_text << std::endl;
                exit(0);
            }
            else if (strcmp(argv[i], "-a") == 0) {
                if      (strcmp(argv[i+1], "random")       == 0) c_gen = CG_RANDOM;
                else if (strcmp(argv[i+1], "dfs")          == 0) c_gen = CG_DFS;
                else if (strcmp(argv[i+1], "dfsimperfect") == 0) c_gen = CG_DFS_IMPERFECT;
                else if (strcmp(argv[i+1], "prims")        == 0) c_gen = CG_PRIMS;
                else {
                    std::cerr << "Invalid algorithm (-a): " << argv[i+1] << std::endl;
                    return false;
                }

            }
            else if (strcmp(argv[i], "-g") == 0) {
                if (strcmp(argv[i+1], "square") == 0) c_grid = CG_SQUARE;
                else {
                    std::cerr << "Invalid grid (-g): " << argv[i+1] << std::endl;
                    return false;
                }

            }
        }
        return true;
    }()) {
        std::cerr << "parsing arguments failed." << std::endl;
        return 1;
    }

    srand(time(NULL));
    if (c_gen == CG_RANDOM) {
        c_gen = static_cast<ChoiceGenerator>(rand() % CG_RANDOM);
    }
    
    maze::GridBase * grid = nullptr;
    maze::GridBase * grid_old = nullptr;
    maze::GenBase  * gen = nullptr;
    
    switch (c_grid) {
        case CG_SQUARE:
            size_t w, h;
            auto ss = std::stringstream(argv[argc-2]);
            ss >> w;
            ss = std::stringstream(argv[argc-1]);
            ss >> h;

            if (w == 0 || h == 0) {
                std::cerr << "Bad arguments to construct the grid: " << argv[argc-2] << " " << argv[argc-1] << std::endl;
            }

            grid = new maze::GridSquare(w,h);
            grid_old = new maze::GridSquare(static_cast<maze::GridSquare&>(*grid));
            break;
    }
    
    switch (c_gen) {
        case CG_DFS:
            gen = new maze::GenDfs(*grid);
            break;
        case CG_DFS_IMPERFECT:
            gen = new maze::GenDfsImperfect(*grid);
            break;
        case CG_PRIMS:
            gen = new maze::GenPrims(*grid);
            break;
        case CG_RANDOM:
            std::cerr << "An exceptional error has occured. Please let someone know how you got here." << std::endl
                      << "CG_RANDOM is not a valid choice while constructing the generator object." << std::endl;
            exit(1);
            break;
    }

    gen->initalise();

    std::cout << grid->print().minimal() << std::endl;


    std::cout << grid_old->delta(*grid) << std::endl << std::endl;
    *grid_old = *grid;
    
    while (!gen->complete()) {
        gen->step();
        std::cout << grid_old->delta(*grid) << std::endl << std::endl;
        *grid_old = *grid;
    }
    grid->clean();
    std::cout << grid->print().minimal() << std::endl;

    delete gen;
    delete grid_old;
    delete grid;

    return 0;
}
