#include "grid_square.h"
#include "solve_dfs.h"
#include "solve_dijkstra.h"
#include "solve_astar.h"
#include <iostream>
#include <stdlib.h>
#include <time.h>
#include <string.h>
#include <functional>

enum ChoiceSolver {
    CS_DFS,
    CS_DIJKSTRA,
    CS_ASTAR,
    CS_RANDOM
};

int main(int argc, char** argv) {
    const char * help_text = (
        "Usage: `solver [-a algorithm]`\n\n"
        "-a algorithm    The algorithm with which to solve the maze.\n"
        "                The default option is \"random\", which will select a random algorithm from the other available options.\n"
        "                Options: dfs, dijkstra, astar, random\n\n"
        "-h/--help       Print this help text.\n"
    );

    auto c_solver = CS_RANDOM;

    if (![&]()->bool {
        for (int i = 1; i < argc; ++i) {
            if (strcmp(argv[i], "-h") == 0 || strcmp(argv[1], "--help") == 0) {
                std::cout << help_text << std::endl;
                exit(0);
            }
            else if (strcmp(argv[i], "-a") == 0) {
                if      (strcmp(argv[i+1], "random")   == 0) c_solver = CS_RANDOM;
                else if (strcmp(argv[i+1], "dfs")      == 0) c_solver = CS_DFS;
                else if (strcmp(argv[i+1], "dijkstra") == 0) c_solver = CS_DIJKSTRA;
                else if (strcmp(argv[i+1], "astar")    == 0) c_solver = CS_ASTAR;
                else {
                    std::cerr << "Invalid algorithm (-a): " << argv[i+1] << std::endl;
                    return false;
                }

            }
        }
        return true;
    }()) {
        std::cerr << "parsing arguments failed." << std::endl;
        return 1;
    }

    srand(time(NULL));
    if (c_solver == CS_RANDOM) {
        c_solver = static_cast<ChoiceSolver>(rand() % CS_RANDOM);
    }

    std::string grid_type_request;
    std::getline(std::cin, grid_type_request, ',');
    maze::GridBase *grid, *grid_old;

    std::function<maze::GridBase*(const maze::GridBase*)> clone_grid_func;

    if (grid_type_request.compare("square") == 0) {
        size_t w,h;
        char discard; // ignore commas
        std::cin >> w >> discard >> h >> discard;

        grid = new maze::GridSquare(w, h);
        grid_old = new maze::GridSquare(w, h);
    
        std::cin >> grid->ingest_block();

    }
    else {
        std::cerr << "Requested an unknown grid type: \"" << grid_type_request << "\"." << std::endl;
        exit(1);
    }

    while(std::cin) {
        std::cin >> grid->ingest_stream();
    }

    std::cout << grid->print().minimal() << std::endl;
    *grid_old = *grid;

    grid->goal_set_last();

    maze::SolveBase * solve = nullptr;
    switch (c_solver) {
        case CS_DFS:
            solve = new maze::SolveDfs(*grid);
            break;
        case CS_DIJKSTRA:
            solve = new maze::SolveDijkstra(*grid);
            break;
        case CS_ASTAR:
            solve = new maze::SolveAstar(*grid);
            break;
        case CS_RANDOM:
            std::cerr << "An exceptional error has occured. Please let someone know how you got here." << std::endl
                      << "CG_RANDOM is not a valid choice while constructing the generator object." << std::endl;
            exit(1);
            break;
        break;
    }

    solve->initalise(0);

    std::cout << grid_old->delta(*grid) << std::endl << std::endl;
    
    while (!solve->complete()) {
        solve->step();
        std::cout << grid_old->delta(*grid) << std::endl << std::endl;
        *grid_old = *grid;
    }

    delete solve;
    delete grid_old;
    delete grid;

    return 0;
}
