#include "solve_dijkstra.h"
#include "exception.h"

namespace maze {

    SolveDijkstra::SolveDijkstra(GridBase& grid) : SolveBase(grid) { }

    void SolveDijkstra::initalise(size_t from) {
        // set the goal's value to 0.
        // mark it as visited.
        // add all its neighbours to the consideration set.

        m_grid.current(from);

        size_t goal = m_grid.goal_get();
        m_grid.value(goal, 0);
        m_grid.visit(goal);
       
        unsigned int neighbour_count;
        uint8_t neighbours = m_grid.unvisited_neighbours(goal, &neighbour_count, true);
       
        for (unsigned int i = 0 ; i < neighbour_count ; ++i) {
            size_t n = m_grid.go(goal, nth_high_bit(neighbours, i));
            m_grid.tag(n);
            m_consider.insert(n);
        }

        m_last_value = 0;
    }

    void SolveDijkstra::step(void) {
        // while there are items in the consideration set. 
        //   add one to the latest value
        //     (assumes all cells have a value of one)
        //   for each item in the consideration set;
        //     set its value to latest value
        //     mark it as visited
        //     add the unvisited neighbours to the consideration set.
        // if the consideration set is empty
        //   move current to the neighbour with the smallest number.

        if (!m_consider.empty()) {
            ++m_last_value;
            for (auto it = m_consider.begin() ; it != m_consider.end() ; ++it) {
                m_grid.value(*it, m_last_value);
                m_grid.tag_clear(*it);
                m_grid.visit(*it);
            }

            std::set<size_t> new_consider;
            for (auto it = m_consider.begin() ; it != m_consider.end() ; ++it) {
                unsigned int neighbour_count;
                uint8_t neighbours = m_grid.unvisited_neighbours(*it, &neighbour_count);

                for (unsigned int i = 0 ; i < neighbour_count ; ++i) {
                    size_t n = m_grid.go(*it, nth_high_bit(neighbours, i));
                    m_grid.tag(n);
                    new_consider.insert(n);
                }
            }
            m_consider = new_consider;
        }
        else {
            size_t current = m_grid.current_get();
            m_grid.tag(current);
            unsigned int neighbour_count;
            uint8_t neighbours = m_grid.visited_neighbours(current, &neighbour_count, true);
            uint16_t value = m_grid.value(current);
            size_t choice = current;
            for (unsigned int i = 0 ; i < neighbour_count ; ++i) {
                size_t n = m_grid.go(current, nth_high_bit(neighbours, i));
                if (m_grid.value(n) < value) {
                    value = m_grid.value(n);
                    choice = n;
                }
            }
            if (choice == current) {
                throw E_GoalNotFound();
            }
            m_grid.current(choice);
            if (m_grid.goal(choice)) {
                m_complete = true;
            }
        }
    }

}
