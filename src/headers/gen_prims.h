#pragma once
#include "gen_base.h"
#include <set>

namespace maze {

    class GenPrims : public GenBase {

        private:
            std::set<size_t> m_set;
            size_t m_visited_count;

        public:
            GenPrims(GridBase& grid);
            virtual void initalise(void);
            virtual void step(void);

    };

}
