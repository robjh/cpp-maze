#pragma once
#include <exception>
#include <string>

struct libevdev;

class Evdev {
    protected:
        struct libevdev *m_dev;
        int m_fd;

        public:
        struct E_EvdevGeneric : public std::exception {
            E_EvdevGeneric();
            const char * what () const throw ();
            std::string m_err;
        };

        struct E_InitEvdev : public E_EvdevGeneric {
            E_InitEvdev(int error_num);
        };

    public:
        Evdev(const char * device);
        void process_events(void);
};
