#pragma once
#include "solve_base.h"
#include <set>

namespace maze {

    class SolveDijkstra : public SolveBase {

        private:
            std::set<size_t> m_consider;
            uint16_t m_last_value;

        public:
            SolveDijkstra(GridBase& grid);
            virtual void initalise(size_t from = 0);
            virtual void step(void);

    };

}
