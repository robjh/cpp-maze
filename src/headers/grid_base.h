#pragma once
#include <cstddef>
#include <cstdint>
#include <ostream>

namespace maze {
    enum Flags {
        WALLFLAGS = 0x00FF,
        CURRENT   = 0x0100,
        VISITED   = 0x0200,
        TAG       = 0x0400,
        GOAL      = 0x0800,
        USEVALUE  = 0x1000,
        VALUEMASK = 0xffff0000
    };

    class GridBase {
        protected:
            size_t m_cells_length;
            size_t m_current;
            size_t m_goal;
            unsigned int* m_cells;
            unsigned int m_wall_count;

        protected:
            GridBase(size_t length, unsigned int wall_count);
            GridBase(const GridBase& original);

        public:
            virtual ~GridBase(void);
            GridBase& operator=(const GridBase& original);
        
        public:
            class Stream_Helper_Ingest {
                private:
                    GridBase& m_grid;
                    bool m_ingest_stream;
                public:
                    Stream_Helper_Ingest(GridBase& grid) : m_grid(grid), m_ingest_stream(false) {}
                    Stream_Helper_Ingest& ingest_stream() {m_ingest_stream = true; return *this;}
                    Stream_Helper_Ingest& ingest_block() {m_ingest_stream = false; return *this;}
                    friend std::istream& operator>>(std::istream& in,  const Stream_Helper_Ingest& h);
            };
            class Stream_Helper_Print {
                public:
                    enum Mode {
                        NONE,
                        MINIMAL,
                        ASCII,
                        STREAM,
                    };
                private:
                    const GridBase& m_grid;
                    Mode m_mode;
                public:
                    Stream_Helper_Print(const GridBase& grid) : m_grid(grid), m_mode(NONE) {}
                    Stream_Helper_Print& minimal() {m_mode = MINIMAL; return *this;}
                    Stream_Helper_Print& ascii() {m_mode = ASCII; return *this;}
                    friend std::ostream& operator<<(std::ostream& out, const Stream_Helper_Print& h);
            };
            class Stream_Helper_Delta {
                private: const GridBase &m_grid1, &m_grid2;
                public:
                    Stream_Helper_Delta(const GridBase& grid1, const GridBase& grid2) : m_grid1(grid1), m_grid2(grid2) {}
                    friend std::ostream& operator<<(std::ostream& out, const Stream_Helper_Delta& h);
            };
        
        public:
            virtual const char * tagline(void) const =0;
            bool operator==(const GridBase& rightside) const;
            bool operator!=(const GridBase& rightside) const;
            virtual void repair_walls(void) =0;
            virtual size_t go(size_t, uint8_t direction) const =0;
            void fill(unsigned int value);
            void clean(void);
            size_t length(void) const;
            void current(size_t index);
            size_t current_get(void);
            void current_clear(void);
            void tag(size_t index);
            void tag_clear(size_t index);
            void tag_clear_all(void);
            void visit(size_t index);
            bool goal(size_t index);
            size_t goal_get(void) const;
            void goal_set(size_t index);
            void goal_set_last(void);
            void goal_clear(void);
            virtual uint8_t opposite_wall(size_t index, uint8_t wallflag) const; 
            void break_wall(size_t index, uint8_t wallflag);
            uint8_t unvisited_neighbours(size_t index, unsigned int * p_count = nullptr, bool heed_walls = true);
            uint8_t visited_neighbours(size_t index, unsigned int * p_count = nullptr, bool heed_walls = false);
            uint8_t walls(size_t index);
            unsigned int wall_count(void) const;
            virtual void cell_position(size_t index, float pos[3]) const;
            void current_cell_position(float pos[3]) const;
            unsigned int cell_by_id(size_t index) const { return m_cells[index]; }
            bool value_isset(size_t index) const;
            uint16_t value(size_t index) const;
            void value(size_t index, uint16_t newvalue);
            virtual unsigned int huristic(size_t from, size_t to) const =0;

            virtual void _print(std::ostream & os, bool minimal=false) const =0;
            virtual void _ascii(std::ostream & os) const =0;
            void _delta(std::ostream & os, const GridBase& grid_alt) const;
            void _ingest_block(std::istream & is);
            void _ingest_stream(std::istream & is);
            Stream_Helper_Print print(bool minimal=false) const;
            Stream_Helper_Print ascii(void) const;
            Stream_Helper_Delta delta(const GridBase& grid_alt);
            Stream_Helper_Ingest ingest_block();
            Stream_Helper_Ingest ingest_stream();

    };

}
