#pragma once
#include "solve_base.h"
#include <list>

namespace maze {

    class SolveDfs : public SolveBase {

        private:
            std::list<size_t> m_stack;
            size_t m_visited_count;

        public:
            SolveDfs(GridBase& grid);
            virtual void initalise(size_t from = 0);
            virtual void step(void);

    };

}
