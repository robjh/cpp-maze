#pragma once
#include "grid_base.h"

namespace maze {
    class GridSquare : public GridBase {
        private:
        typedef enum {
            RIGHT = 0x01,
            DOWN  = 0x02,
            LEFT  = 0x04,
            UP    = 0x08,
            ALL   = 0x0F
        } Direction;
        size_t m_width;

        public:
            GridSquare(size_t width, size_t height);
            virtual size_t go(size_t index, uint8_t direction) const;
            virtual void repair_walls(void);
            virtual void cell_position(size_t index, float pos[3]) const;
            virtual unsigned int huristic(size_t from, size_t to) const;
            virtual void _print(std::ostream & os, bool minimal=false) const;
            virtual void _ascii(std::ostream & os) const;
            virtual const char * tagline(void) const;
    };
}
