#pragma once
#include "grid_base.h"

namespace maze {
    class GenBase {
        protected:
            GridBase& m_grid;
            bool m_complete;
        
        protected:
            GenBase(GridBase& grid);
            uint8_t nth_high_bit(uint8_t flags, unsigned int bitcount) const;
        
        public:
            virtual ~GenBase(void);
            bool complete(void);
            void generate(void);
            virtual void initalise(void) =0;
            virtual void step(void) =0;
    };
}
