#pragma once
#include "grid_base.h"

namespace maze {
    class SolveBase {
        protected:
            GridBase& m_grid;
            bool m_complete;
        
        protected:
            SolveBase(GridBase& grid);
            uint8_t nth_high_bit(uint8_t flags, unsigned int bitcount) const;
        
        public:
            virtual ~SolveBase(void);
            bool complete(void);
            void solve(void);
            virtual void initalise(size_t from) =0;
            virtual void step(void) =0;
    };
}
