#pragma once
#include <exception>

namespace maze {

    class E_OutOfBounds : public std::exception {
        const char * what () const throw () {
            return "The index was out of bounds.";
        }
    };

    class E_GridTopologyMissmatch : public std::exception {
        const char * what () const throw () {
            return "Cannot equate the two grids, as they appear to be topologically distinct.";
        }
    };

    class E_UnexpectedEof: public std::exception {
        const char * what () const throw () {
            return "Attempted to do a block read, but the buffer didn't contain enough data to populate the entire array.";
        }
    };

    class E_UnexpectedData: public std::exception {
        const char * what () const throw () {
            return "After reading enough data to populate the array, additional data was found in the buffer.";
        }
    };

    class E_NoMoreBits: public std::exception {
        const char * what () const throw () {
            return "An Nth bit was requested, but fewer bits were set in the register.";
        }
    };

    class E_GoalNotFound: public std::exception {
        const char * what () const throw () {
            return "The goal was not found. This could mean the grid is not a perfect maze, or the goal does not exist.";
        }
    };

    class E_OptionalFunctionUnimplemented: public std::exception {
        const char * what () const throw () {
            return "Optional Function Unimplemented.";
        }
    };

    class E_NoCurrentSet: public std::exception {
        const char * what () const throw () {
            return "The \"current\" cell was requested, but no cell is currently the current cell.";
        }
    };

    class E_NoGoalSet: public std::exception {
        const char * what () const throw () {
            return "The \"goal\" cell was requested, but no cell is currently the goal cell.";
        }
    };

}
