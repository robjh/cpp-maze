#pragma once
#include "gen_base.h"
#include <list>

namespace maze {

    class GenDfs : public GenBase {

        private:
            std::list<size_t> m_stack;
            size_t m_visited_count;

        public:
            GenDfs(GridBase& grid);
            virtual void initalise(void);
            virtual void step(void);

    };

}
