#pragma once
#include "gen_base.h"
#include <list>

namespace maze {

    class GenDfsImperfect : public GenBase {

        private:
            std::list<size_t> m_stack;
            bool m_backtracking;

        public:
            GenDfsImperfect(GridBase& grid);
            virtual void initalise(void);
            virtual void step(void);

    };

}
