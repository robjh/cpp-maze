#pragma once
#include "solve_base.h"
#include <list>

namespace maze {

    class SolveAstar : public SolveBase {

        public:
            struct Cell {
                size_t index;
                size_t parent;
                int f;
                int g;
                bool operator<(const Cell& other) const {
                    return f > other.f; // smallest first
                }
                bool operator==(const Cell& other) const {
                    return index == other.index;
                }
            };
        private:
            std::list<Cell> m_open;
            std::list<Cell> m_close;
            std::list<size_t> m_path;
            bool m_path_found;

            bool in_open(size_t index, Cell** cellpp = nullptr);
            Cell lowest_f();
            void remove(size_t index);
            void construct_path(Cell& goal);

        public:
            SolveAstar(GridBase& grid);
            virtual void initalise(size_t from = 0);
            virtual void step(void);

    };

}
