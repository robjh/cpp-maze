#include "grid_base.h"
#include "exception.h"
#include <cstring>
#include <istream>
#include <sstream>

namespace maze {

    GridBase::GridBase(size_t length, unsigned int wall_count) : m_cells_length(length), m_current(length), m_goal(length), m_wall_count(wall_count) {
        m_cells = new unsigned int[length];
    }

    GridBase::GridBase(const GridBase& original) : m_cells_length(original.m_cells_length), m_current(original.m_current), m_goal(original.m_goal), m_wall_count(original.m_wall_count) {
        m_cells = new unsigned int[m_cells_length];
        memcpy(m_cells, original.m_cells, sizeof(unsigned int) * m_cells_length);
    }

    GridBase& GridBase::operator=(const GridBase& original) {
        if (m_wall_count != original.m_wall_count) {
            throw E_GridTopologyMissmatch();
        }
        if (m_cells_length != original.m_cells_length) {
            delete m_cells;
            m_cells_length = original.m_cells_length;
            m_cells = new unsigned int[m_cells_length];
        }
        m_current = original.m_current;
        m_goal = original.m_goal;
        memcpy(m_cells, original.m_cells, sizeof(unsigned int) * m_cells_length);
        return *this;
    }

    GridBase::~GridBase(void) {
        delete m_cells;
    }
  
    bool GridBase::operator==(const GridBase& rs) const {
        if (m_cells_length != rs.m_cells_length) return false;
        for (size_t i = 0 ; i < m_cells_length ; ++i) {
            if (m_cells[i] != rs.m_cells[i]) return false;
        }
        return true;
    }
    bool GridBase::operator!=(const GridBase& rs) const {
        return !this->operator==(rs);
    }

    void GridBase::fill(unsigned int value) {
        for (size_t i = 0 ; i < m_cells_length ; ++i) {
            m_cells[i] = value;
        }
    }
    
    void GridBase::clean(void) {
        for (size_t i = 0 ; i < m_cells_length ; ++i) {
            m_cells[i] &= Flags::WALLFLAGS;
        }
        m_current = m_cells_length;
    }
    
    size_t GridBase::length(void) const {
        return m_cells_length;
    }

    void GridBase::current(size_t index) {
        if (m_current < m_cells_length) {
            m_cells[m_current] &= ~CURRENT;
        }
        m_cells[index] |= CURRENT;
        m_current = index;
    }

    void GridBase::current_clear() {
        if (m_current < m_cells_length) {
            m_cells[m_current] &= ~CURRENT;
        }
        m_current = m_cells_length;
    }

    size_t GridBase::current_get(void) {
        if (m_current >= m_cells_length) {
            throw E_NoCurrentSet();
        }
        return m_current;
    }

    void GridBase::tag(size_t index) {
        m_cells[index] |= TAG;
    }

    void GridBase::tag_clear(size_t index) {
        m_cells[index] &= ~TAG;
    }

    void GridBase::tag_clear_all(void) {
        for (size_t i = 0 ; i < m_cells_length ; ++i) {
            tag_clear(i);
        }
    }

    void GridBase::visit(size_t index) {
        m_cells[index] |= VISITED;
    }

    bool GridBase::goal(size_t index) {
        return (m_cells[index] & GOAL) == GOAL;
    }
    size_t GridBase::goal_get(void) const {
        if (m_goal == m_cells_length) {
            throw E_NoGoalSet();
        }
        return m_goal;
    }
    void GridBase::goal_set(size_t index) {
        goal_clear();
        m_cells[index] |= GOAL;
        m_goal = index;
    }
    void GridBase::goal_set_last() {
        goal_set(m_cells_length-1);
    }
    void GridBase::goal_clear() {
        if (m_goal < m_cells_length) {
            m_cells[m_goal] &= ~GOAL;
        }
        m_goal = m_cells_length;
    }
    
    uint8_t GridBase::opposite_wall(size_t index, uint8_t wallflag) const {
        // given a bit in `wallflag`, this function will return the opposite bit for the same wall from the perspective of the neighbour.
        // this default implementation makes the following assumptions;
        //   - There are an even number of doors.
        //   - All rooms behave in the same way.
        //   - The bits map to walls in a clockwise fashion.
        const int half_wall_count = m_wall_count / 2;
        return ((wallflag & ((1 << half_wall_count) - 1)) > 0) ? wallflag << half_wall_count : wallflag >> half_wall_count;
    } 

    void GridBase::break_wall(size_t index, uint8_t wallflag) {
        const size_t neighbour = go(index, wallflag);
        m_cells[index] &= ~wallflag;
        m_cells[neighbour] &= ~opposite_wall(index, wallflag);
    }

    uint8_t GridBase::unvisited_neighbours(size_t index, unsigned int * p_count, bool heed_walls) {
        unsigned int wallmask = heed_walls ? m_cells[index] : 0;
        uint8_t ret = 0;
        if (p_count != nullptr) *p_count = 0;

        for (unsigned int i = 0 ; i < m_wall_count ; ++i) {
            try {
                const uint8_t flag = 0x01 << i;
                if ((wallmask & flag) == 0 && (m_cells[go(index, flag)] & VISITED) == 0) {
                    if (p_count != nullptr) ++(*p_count);
                    ret |= flag;
                }
            }
            catch (E_OutOfBounds& e) {
                // this will happen when not heeding walls in cells against the boundaries of the grid.
                // the correct thing to do is to not set the flag, and therefore do nothing here.
            }
        }

        return ret;
    }
    uint8_t GridBase::visited_neighbours(size_t index, unsigned int * p_count, bool heed_walls) {
        unsigned int wallmask = heed_walls ? m_cells[index] : 0;
        uint8_t ret = 0;
        if (p_count != nullptr) *p_count = 0;

        for (unsigned int i = 0 ; i < m_wall_count ; ++i) {
            try {
                const uint8_t flag = 0x01 << i;
                if ((wallmask & flag) == 0 && (m_cells[go(index, flag)] & VISITED) == VISITED) {
                    if (p_count != nullptr) ++(*p_count);
                    ret |= flag;
                }
            }
            catch (E_OutOfBounds& e) {
                // this will happen when not heeding walls in cells against the boundaries of the grid.
                // the correct thing to do is to not set the flag, and therefore do nothing here.
            }
        }

        return ret;
    }
    uint8_t GridBase::walls(size_t index) {
        return m_cells[index] & WALLFLAGS;
    }
    unsigned int GridBase::wall_count(void) const {
        return m_wall_count;
    }
    void GridBase::cell_position(size_t index, float pos[3]) const {
        throw E_OptionalFunctionUnimplemented();
    }
    
    void GridBase::current_cell_position(float pos[3]) const {
        if (m_current == m_cells_length) throw E_NoCurrentSet();
        cell_position(m_current, pos);
    }

    bool GridBase::value_isset(size_t index) const {
        return (m_cells[index] & USEVALUE) == USEVALUE;
    }
    uint16_t GridBase::value(size_t index) const {
        return (m_cells[index] & VALUEMASK) >> 16;
    }
    void GridBase::value(size_t index, uint16_t newvalue) {
        m_cells[index] |= (newvalue << 16) | USEVALUE;
    }

    void GridBase::_delta(std::ostream & os, const GridBase& grid_alt) const {
        for (size_t i = 0 ; i < m_cells_length ; ++i) {
            if (m_cells[i] != grid_alt.m_cells[i]) {
                os << i << "," << grid_alt.m_cells[i] << ",";
            }
        }
    }

    void GridBase::_ingest_block(std::istream & is) {
        
        // enable throwing an exception on EOF, since we expect to not hit it here.
        auto exception_state = is.exceptions();
        is.exceptions(exception_state | std::istream::eofbit);

        // attempt to read enough data to fill the array
        try {
            char comma;
            for (size_t i = 0 ; i < m_cells_length ; ++i) {
                is >> m_cells[i] >> comma;
            }
        } catch (const std::ios_base::failure& fail) {
            throw E_UnexpectedEof();
        }

        // put exceptions back to how it was before
        is.exceptions(exception_state);

        // if everything worked as expected, the remainder of the current line will be empty
        //   and the next line will also be empty.
        std::string line;
        for (size_t i = 0 ; i < 2 ; ++i) {
            std::getline(is, line);
            if (line.length() > 0) {
                throw E_UnexpectedData();
            }
        }

        // populate flags from partial data.
        repair_walls();

    }
    void GridBase::_ingest_stream(std::istream & is) {
        std::string line;
        char comma;
        
        while (true) {
            size_t index;
            unsigned int value;
            std::getline(is, line);
            if (line.compare("") == 0) break;
            if (line.rfind(tagline(), 0) == 0) {
                _ingest_block(is);
            }
            else {
                std::stringstream ss(line);
                while (ss) {
                    ss >> index >> comma >> value >> comma;
                    m_cells[index] = value;
                    
                    // properly manage m_current
                    if ((value & CURRENT) == CURRENT) {
                        m_current = index;
                    } else if (index == m_current && ((value & CURRENT) == 0)) {
                        m_current = m_cells_length;
                    }
                    // properly manage m_goal
                    if ((value & GOAL) == GOAL) {
                        m_goal = index;
                    } else if (index == m_goal && ((value & GOAL) == 0)) {
                        m_goal = m_cells_length;
                    }
                }
            }
        }
        
    }

    GridBase::Stream_Helper_Print GridBase::print(bool minimal) const {
        if (minimal)
            return GridBase::Stream_Helper_Print(*this).minimal();
        else
            return GridBase::Stream_Helper_Print(*this);
    }
    GridBase::Stream_Helper_Print GridBase::ascii() const {
        return GridBase::Stream_Helper_Print(*this).ascii();
    }
    GridBase::Stream_Helper_Delta GridBase::delta(const GridBase& grid_alt) {
        return GridBase::Stream_Helper_Delta(*this, grid_alt);
    }
    GridBase::Stream_Helper_Ingest GridBase::ingest_block() {
        return GridBase::Stream_Helper_Ingest(*this).ingest_block();
    }
    GridBase::Stream_Helper_Ingest GridBase::ingest_stream() {
        return GridBase::Stream_Helper_Ingest(*this).ingest_stream();
    }
    
    std::ostream& operator<<(std::ostream& os, const GridBase::Stream_Helper_Print& h) {
        switch (h.m_mode) {
            case GridBase::Stream_Helper_Print::ASCII:
                h.m_grid._ascii(os);
                break;
            default:
                h.m_grid._print(os, h.m_mode == GridBase::Stream_Helper_Print::MINIMAL);
                break;
        }
        return os;
    }
    std::ostream& operator<<(std::ostream& os, const GridBase::Stream_Helper_Delta& h) {
        h.m_grid1._delta(os, h.m_grid2);
        return os;
    }
    std::istream& operator>>(std::istream& is, const GridBase::Stream_Helper_Ingest& h) {
        if (h.m_ingest_stream) {
            h.m_grid._ingest_stream(is);
        }
        else {
            h.m_grid._ingest_block(is);
        }
        return is;
    }
    
}
