#include "gen_dfs.h"
#include <stdlib.h>

namespace maze {

    GenDfs::GenDfs(GridBase& grid) : GenBase(grid) { }

    void GenDfs::initalise(void) {
       const size_t last_cell = m_grid.length() - 1; 
       m_visited_count = 1;
       m_grid.current(last_cell);
       m_grid.visit(last_cell);
       m_grid.tag(last_cell);
       m_stack.push_back(last_cell);
    }

    void GenDfs::step(void) {
        // if there are unvisited neighbours,
        //   choose one at random,
        //   break the wall,
        //   visit and make it current,
        //   add it to the stack.
        // else, pop from the stack and make the new head current.       
        //   if the stack is empty, the algorithm is complete.

        const size_t cur = m_stack.back();
        unsigned int candidate_count;
        uint8_t candidate_neighbours = m_grid.unvisited_neighbours(cur, &candidate_count, false);

        if (candidate_count > 0) {
            uint8_t next_direction = nth_high_bit(candidate_neighbours, rand() % candidate_count);
            const size_t next = m_grid.go(cur, next_direction);

            m_grid.break_wall(cur, next_direction);
            m_grid.visit(next);
            m_grid.tag(next);
            m_grid.current(next);
            m_stack.push_back(next);
            if (++m_visited_count == m_grid.length()) {
                m_complete = true;
            }
        }

        else {
            m_grid.tag_clear(cur);
            m_stack.pop_back();
            m_grid.current(m_stack.back());
        }

    }

}
