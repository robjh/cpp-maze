#include "grid_square.h"
#include "exception.h"
#include <sstream>
#include <iomanip>
#include <cmath>

#define COORD2I(x, y) (( m_width * (y) + (x) ))

namespace maze {
    const char * GridSquare::tagline(void) const {
        return "square";
    }
    GridSquare::GridSquare(size_t w, size_t h) : GridBase(w*h, 4), m_width(w) {       
        fill(Direction::ALL);
    }

    size_t GridSquare::go(size_t index, uint8_t direction) const {
        const int x = index % m_width;
        if ((Direction::DOWN  & direction) > 0) index += m_width;
        if ((Direction::UP    & direction) > 0) index -= m_width;
        if ((Direction::RIGHT & direction) > 0) {
            if (x+1 > (int)m_width-1) throw E_OutOfBounds();
            index += 1;
        }
        if ((Direction::LEFT  & direction) > 0) {
            if (x-1 < 0) throw E_OutOfBounds();
            index -= 1;
        }
        
        if (index >= m_cells_length) throw E_OutOfBounds();

        return index;
    }
 
    void GridSquare::repair_walls() {
        const size_t last_row = m_cells_length - m_width;
        const size_t height = m_cells_length / m_width;

        // ensure the grid has a well defined perimeter
        for (size_t i = 0 ; i < m_width ; ++i) {
            m_cells[i] |= Direction::UP;
            m_cells[last_row + i] |= Direction::DOWN;
        }
        for (size_t i = 0 ; i < height ; ++i) {
            m_cells[i * m_width] |= Direction::LEFT;
            m_cells[(i+1) * m_width - 1] |= Direction::RIGHT;
        }

        // construct UP and LEFT flags from neighbor's DOWN and RIGHT flags.
        for (size_t y = 0 ; y < height ; ++y) for (size_t x = 0 ; x < m_width ; ++x) {
            if (x > 0 && (m_cells[COORD2I(x-1, y)] & Direction::RIGHT) > 0) {
                m_cells[COORD2I(x,y)] |= Direction::LEFT;
            }
            if (y > 0 && (m_cells[COORD2I(x,y-1)] & Direction::DOWN) > 0) {
                m_cells[COORD2I(x,y)] |= Direction::UP;
            }
        }
    }
    
    void GridSquare::cell_position(size_t index, float pos[3]) const {
        // compute the centre of the room 
        const float room_size = 2.0f;
        const size_t height = m_cells_length / m_width;
        const float offset_x = (room_size * m_width - room_size) / 2;
        const float offset_y = (room_size * height - room_size) / 2;
        const size_t x = index % m_width;
        const size_t y = index / m_width;
        pos[0] = room_size * x - offset_x;
        pos[1] = 0.0f;
        pos[2] = room_size * y - offset_y;
    }

    unsigned int GridSquare::huristic(size_t from, size_t to) const {
        const int x = to % m_width - from % m_width;
        const int y = to / m_width - from / m_width;
        return (x*x+y*y);
    }
 
    void GridSquare::_print(std::ostream & os, bool minimal) const {
        os << "square," << m_width << "," << m_cells_length / m_width << "," << std::endl;
        unsigned int mask = minimal ? (Direction::RIGHT | Direction::DOWN) : 0xffffffff;
        for (size_t i = 0 ; i < m_cells_length ; ++i) {
            os << (m_cells[i] & mask) << ",";
            if (i % m_width == m_width - 1) {
                os << std::endl;
            }
        }
    }

    void GridSquare::_ascii(std::ostream & os) const {
        const size_t height = m_cells_length / m_width;
        for (size_t y = 0 ; y < height ; ++y) {
            std::stringstream second_line;
            for (size_t x = 0 ; x < m_width ; ++x) {
                const size_t i = COORD2I(x,y);
                char crumb = ' ';
                bool print_value = false;
                if      (m_cells[i] & CURRENT) crumb = 'C';
                else if (m_cells[i] & GOAL)    crumb = 'G';
                else if (m_cells[i] & TAG)     crumb = 'o';
                else if (m_cells[i] & USEVALUE) print_value = true;
                else if (m_cells[i] & VISITED) crumb = '-';
                os << "+" << (((m_cells[i] & Direction::UP) > 0) ? "---" : "   ");
                second_line << (((m_cells[i] & Direction::LEFT) > 0) ? "|" : " ");
                if (print_value) {
                    uint16_t v = value(i) > 999 ? 999 : value(i);
                    second_line << std::setfill(' ') << std::setw(3) << v;
                }
                else {
                    second_line << " " << crumb << " ";
                }
            }
            os << "+" << std::endl << second_line.str() << "|" << std::endl;
        }

        for (size_t x = 0 ; x < m_width ; ++x) {
            os << "+---";
        }
        os << "+";
    }
}
