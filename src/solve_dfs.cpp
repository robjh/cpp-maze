#include "solve_dfs.h"
#include "exception.h"

namespace maze {

    SolveDfs::SolveDfs(GridBase& grid) : SolveBase(grid) { }

    void SolveDfs::initalise(size_t from) {
       m_visited_count = 1;
       m_grid.current(from);
       m_grid.visit(from);
       m_grid.tag(from);
       m_stack.push_back(from);
    }

    void SolveDfs::step(void) {
        // if there are unvisited neighbours,
        //   choose the first one,
        //   visit and make it current,
        //   add it to the stack.
        // else, pop from the stack and make the new head current.       
        //   if the current tile is marked as the exit, mark as complete.
        //   if the stack is empty, throw an error.

        const size_t cur = m_stack.back();
        unsigned int candidate_count;
        uint8_t candidate_neighbours = m_grid.unvisited_neighbours(cur, &candidate_count, true);

        if (candidate_count > 0) {
            uint8_t next_direction = nth_high_bit(candidate_neighbours, rand() % candidate_count);
            const size_t next = m_grid.go(cur, next_direction);

            m_grid.visit(next);
            m_grid.tag(next);
            m_grid.current(next);
            m_stack.push_back(next);
            if (m_grid.goal(next)) {
                m_complete = true;
            }
        }

        else {
            m_grid.tag_clear(cur);
            m_stack.pop_back();
            if (m_stack.empty()) {
                throw E_GoalNotFound();
            }
            m_grid.current(m_stack.back());
        }

    }

}
