extern "C" {
#include <GL/glew.h>
#include <GL/glut.h>
}
#include <glm/glm.hpp>
#include <glm/ext.hpp>
#include <iostream>
#include <sstream>

#include "grid_square.h"
#include "exception.h"

enum {
    W = 0,
    H
};

enum {
    ATTR_POSITION,
    ATTR_COLOUR,
    ATTR_NORMAL,
    ATTR_TEXTURE,
};

class Scene {
    private:
        float m_angle;
        int m_windowsize[2];
        GLuint m_triangleBuffer;
        GLuint m_vertarr;
        GLuint m_program;
        maze::GridBase * m_grid;
        glm::mat4x4 m_mat_projection, m_mat_view;
        glm::vec3 m_cam_pos;

    public:
        Scene() : m_angle(0.0f) {
            windowsize_inform(800,600);
        }
        void windowsize_inform(int x, int y) {
            float ratio = (float)x / y;
            m_windowsize[W] = x; m_windowsize[H] = y;
            m_mat_projection = glm::perspective(45.0f, ratio, 0.1f, 100.0f);
        }
        const int* windowsize() const { return m_windowsize; }

        void init(void);
        void init_shaders(void);
        void init_grid(void);
        void teardown(void);

        void update(float delta);
        void render(void);
        void render_room(const GLint matrix_model_loc, size_t index);
};


struct VertexFormat {
    glm::vec3 pos;
    glm::vec3 normal;
    glm::vec3 colour;
    glm::vec2 tex;
};

void Scene::init() {
    // runs after the GL context has been created.

    glEnable(GL_CULL_FACE);
    glDepthFunc(GL_LESS);
    glEnable(GL_DEPTH_TEST);
    glEnable(GL_MULTISAMPLE);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    glEnable(GL_BLEND);

    init_grid();
    init_shaders();

    VertexFormat verts[] = {
        {glm::vec3(-1.0f, -1.0f, 0.0f), glm::vec3(0.0f, 0.0f, 1.0f), glm::vec3(0.9f, 0.0f, 0.0f), glm::vec2()},
        {glm::vec3(-1.0f,  1.0f, 0.0f), glm::vec3(0.0f, 0.0f, 1.0f), glm::vec3(0.0f, 0.9f, 0.0f), glm::vec2()},
        {glm::vec3( 1.0f,  1.0f, 0.0f), glm::vec3(0.0f, 0.0f, 1.0f), glm::vec3(0.0f, 0.0f, 0.9f), glm::vec2()},
        {glm::vec3( 1.0f, -1.0f, 0.0f), glm::vec3(0.0f, 0.0f, 1.0f), glm::vec3(0.7f, 0.0f, 0.0f), glm::vec2()}
    };

    const VertexFormat* nullvert = NULL;

    glGenVertexArrays(1, &m_vertarr);
    glGenBuffers(1, &m_triangleBuffer);

    glBindVertexArray(m_vertarr);
    glBindBuffer(GL_ARRAY_BUFFER, m_triangleBuffer);
    glBufferData(GL_ARRAY_BUFFER, sizeof(verts), verts, GL_STATIC_DRAW);
    glVertexAttribPointer(ATTR_POSITION, 3, GL_FLOAT, GL_FALSE, sizeof(VertexFormat), &nullvert->pos);
    glEnableVertexAttribArray( ATTR_POSITION );
    glVertexAttribPointer(ATTR_COLOUR, 3, GL_FLOAT, GL_FALSE, sizeof(VertexFormat), &nullvert->colour);
    glEnableVertexAttribArray( ATTR_COLOUR );
    glVertexAttribPointer(ATTR_NORMAL, 3, GL_FLOAT, GL_TRUE, sizeof(VertexFormat), &nullvert->normal);
    glEnableVertexAttribArray( ATTR_NORMAL );
    glVertexAttribPointer(ATTR_TEXTURE, 2, GL_FLOAT, GL_TRUE, sizeof(VertexFormat), &nullvert->tex);
    glEnableVertexAttribArray( ATTR_TEXTURE );
    glBindBuffer(GL_ARRAY_BUFFER, 0);
}

void Scene::init_shaders() {
    std::stringstream src_vss = std::stringstream();

    src_vss <<
        "#version 320 es" << std::endl <<
        "layout(location = " << ATTR_POSITION << ") in vec3 vertex_position;"
        "layout(location = " << ATTR_NORMAL   << ") in vec3 vertex_normal;"
        "layout(location = " << ATTR_COLOUR   << ") in vec3 vertex_colour;"
        "layout(location = " << ATTR_TEXTURE  << ") in vec2 vertex_texture;"

        "uniform mat4 matrix_projectionview;"
        "uniform mat4 matrix_model;"

        "out vec3 colour;"
        "out vec3 normal;"
        "out vec3 fragpos;"

        "void main() {"
        "  colour = vertex_colour;"
        "  normal = vertex_normal;"
        "  fragpos = vec3(matrix_model * vec4(vertex_position, 1.0));"
        "  gl_Position = matrix_projectionview * matrix_model * vec4(vertex_position, 1.0);"
        "}";
    std::string src_vs = src_vss.str();
    const GLchar * src_v = src_vs.c_str();
    const GLchar * src_f = (
        "#version 320 es\n"
        "lowp in vec3 colour;"
        "lowp in vec3 normal;"
        "lowp in vec3 fragpos;"
        "lowp uniform vec3 light_pos;"

        "lowp out vec4 frag_colour;"

        "void main() {"
        "  lowp vec3 n_normal = normalize(normal);"
        "  lowp vec3 n_lightdir = normalize(light_pos - fragpos);"
        "  lowp float diff = max(dot(n_normal, n_lightdir), 0.0);"
        "  lowp vec3 diffuse = diff * vec3(1.0, 1.0, 1.0);"

        "  lowp vec3 ambient = 0.1 * colour;"
        "  lowp vec3 result = (ambient + diffuse) * colour;"
        "  frag_colour = vec4(result, 1.0);"
        "}"
    );

    GLuint vshader = glCreateShader(GL_VERTEX_SHADER);
    glShaderSource(vshader, 1, &src_v, NULL);
    glCompileShader(vshader);

    GLuint fshader = glCreateShader(GL_FRAGMENT_SHADER);
    glShaderSource(fshader, 1, &src_f, NULL);
    glCompileShader(fshader);

    GLint compiled;
    glGetShaderiv(vshader, GL_COMPILE_STATUS, &compiled);
    if (compiled != GL_TRUE) {
        GLsizei log_length = 0;
        GLchar message[1024];
        glGetShaderInfoLog(vshader, 1024, &log_length, message);
        std::cerr << "Error compiling vertex shader: " << message << std::endl;
        exit(1);
    }
    glGetShaderiv(fshader, GL_COMPILE_STATUS, &compiled);
    if (compiled != GL_TRUE) {
        GLsizei log_length = 0;
        GLchar message[1024];
        glGetShaderInfoLog(fshader, 1024, &log_length, message);
        std::cerr << "Error compiling fragment shader: " << message << std::endl;
        exit(1);
    }
    m_program = glCreateProgram();
    glAttachShader(m_program, vshader);
    glAttachShader(m_program, fshader);
    glLinkProgram(m_program);
    glGetProgramiv(m_program, GL_LINK_STATUS, &compiled);
    if (compiled != GL_TRUE) {
        GLsizei log_length = 0;
        GLchar message[1024];
        glGetProgramInfoLog(m_program, 1024, &log_length, message);
        std::cout << "Error shader program linking failed: " << message << std::endl;
        exit(1);
    }
    glDeleteShader(vshader);
    glDeleteShader(fshader);
}

void Scene::init_grid(){
    std::string grid_type_request;
    std::getline(std::cin, grid_type_request, ',');

    if (grid_type_request.compare("square") == 0) {
        size_t w,h;
        char discard; // ignore commas
        std::cin >> w >> discard >> h >> discard;
        m_grid = new maze::GridSquare(w, h);
        std::cin >> m_grid->ingest_block();
    }
    else {
        std::cerr << "Requested an unknown grid type: \"" << grid_type_request << "\"." << std::endl;
        exit(1);
    }

    size_t cur;
    for (;;) {
        try {
            cur = m_grid->current_get();
            break;
        } catch (maze::E_NoCurrentSet &e) {
            // read from STDIN until the current bit is set,
            // or lose stdin and throw the error up the stack.
            if (std::cin.eof()) throw e;
            std::cin >> m_grid->ingest_stream();
        }
    }
    m_grid->cell_position(cur, glm::value_ptr(m_cam_pos));
        std::cout << m_grid->print().ascii() <<
        "(" << m_cam_pos[0] << "," << m_cam_pos[2] << ")" << std::endl;
}

void Scene::teardown() {
    glDeleteVertexArrays(1, &m_vertarr);
    glDeleteBuffers(1, &m_triangleBuffer);
    glDeleteProgram(m_program);
    delete m_grid;
}

void Scene::update(float delta) {

    
    // how far to the next target? if its close, then get the next target.
    auto vec_target_pos = glm::vec3(1.0f);
    m_grid->current_cell_position(glm::value_ptr(vec_target_pos));
    auto vec_totarget = vec_target_pos - m_cam_pos;
    const auto forward = glm::vec3(0.0f, 0.0f, 1.0f);

    if (glm::length(vec_totarget) < 0.01f) {
        if (std::cin.eof()) return; // all done.
        std::cin >> m_grid->ingest_stream();
        std::cout << m_grid->print().ascii() <<
        "(" << m_cam_pos[0] << "," << m_cam_pos[2] << ")" << std::endl;
        m_grid->current_cell_position(glm::value_ptr(vec_target_pos));
        vec_totarget = vec_target_pos - m_cam_pos;
    }

    // are we facing the target? if not, rotate.
    else {
        auto vec_to_target_normal = glm::normalize(vec_totarget);
        float target_angle = glm::acos(glm::dot(forward, vec_to_target_normal));
        if (vec_to_target_normal[0] < 0.0f) target_angle *= -1;


        if (m_angle < target_angle) {
            if (m_angle + delta*1.5f >= target_angle) {
                m_angle = target_angle;
            } else {
                m_angle += delta*1.5f;
            }
        } else if (m_angle > target_angle) {
            if (m_angle - delta*1.5f <= target_angle) {
                m_angle = target_angle;
            } else {
                m_angle -= delta*1.5f;
            }
        }
        
        // and, progress towards the target.
        auto vec_move = vec_to_target_normal * delta;
        if (glm::length(vec_move) > glm::length(vec_totarget)) {
            m_cam_pos += vec_totarget;
        } else {
            m_cam_pos += vec_move;
        }
    }


    // Set the camera, it is our player.
    auto cam_dir = glm::vec4(forward, 1.0f);
    auto spin = glm::rotate(glm::identity<glm::mat4x4>(), m_angle, glm::vec3(0.0f, 1.0f, 0.0f));
    cam_dir = spin * cam_dir;

    m_mat_view = glm::lookAt(
        m_cam_pos,
        m_cam_pos + glm::vec3(cam_dir),
        glm::vec3(0.0f, 1.0f, 0.0f)
    );

}


void Scene::render() {
    glUseProgram(m_program);
    GLint matrix_projview_loc = glGetUniformLocation(m_program, "matrix_projectionview");
    GLint matrix_model_loc = glGetUniformLocation(m_program, "matrix_model");
    GLint light_location = glGetUniformLocation(m_program, "light_pos");

    auto mat_projview = m_mat_projection * m_mat_view;
    glUniformMatrix4fv(matrix_projview_loc, 1, GL_FALSE, glm::value_ptr(mat_projview));


    const glm::vec3 light = glm::vec3(50.0f, -50.0f, 50.0f);
    glUniform3fv(light_location, 1, glm::value_ptr(light));

    for (size_t i = 0 ; i < m_grid->length() ; ++i) {
        render_room(matrix_model_loc, i);
    }

}
void Scene::render_room(const GLint matrix_model_loc, size_t index) {
    glm::vec3 cell_pos(1.0f);
    m_grid->cell_position(index, glm::value_ptr(cell_pos));
//    std::cout << index << ": " << cell_pos[0] << " x " << cell_pos[2] << std::endl;
    auto mat_translate_wall = glm::translate(glm::mat4x4(1.0f), glm::vec3(0.0f,0.0f,1.0f));
    auto mat_room_pos = glm::translate(glm::identity<glm::mat4x4>(), cell_pos);
    unsigned int flags = m_grid->cell_by_id(index);

    for (int i = 0 ; i < 4 ; ++i) {
        if ((flags & (0x01 << i)) == 0) continue;
        auto mat_wall_rotation = glm::rotate(glm::identity<glm::mat4x4>(), glm::pi<float>()/-2 * (i+3), glm::vec3(0.0f, 1.0f, 0.0f));
        auto mat_model = mat_room_pos * mat_wall_rotation * mat_translate_wall;
        glUniformMatrix4fv(matrix_model_loc, 1, GL_FALSE, glm::value_ptr(mat_model));
        glDrawArrays(GL_TRIANGLE_FAN, 0, 4);
    }
}



// ----------------------------------------------------------------------------------------------------
// needed because converting c++ lambdas with captures to a c function pointer is annoyingly difficult.
struct GLOBAL {
    Scene scene;
    int time_mil_old;
} g;

int main(int argc, char **argv) {

    // init GLUT and create window
    glutInit(&argc, argv);
    glutInitDisplayMode(GLUT_DEPTH | GLUT_DOUBLE | GLUT_RGBA);
    glutInitWindowPosition(100,100);
    glutInitWindowSize(g.scene.windowsize()[W], g.scene.windowsize()[H]);
    glutCreateWindow("Maze Renderer GL");

    GLenum err = glewInit();
    if (GLEW_OK != err) {
        /* Problem: glewInit failed, something is seriously wrong. */
        std::cerr << "Error: " << glewGetErrorString(err) << std::endl;
        return 1;
    }
    if (!glewIsSupported("GL_VERSION_3_0")) {
        std::cerr << "Error: GL too old." << std::endl;
        return 1;
    }

    // register callbacks
    glutReshapeFunc([](int w, int h) {
        // Prevent a divide by zero
        if (h == 0) h = 1;
        g.scene.windowsize_inform(w, h);
        glViewport(0, 0, w, h);
    });
    auto cb_render = [](){
        // compute how much time has elapsed since the last update, and update the scene.
        int time_mil = glutGet(GLUT_ELAPSED_TIME);
        g.scene.update((float)(time_mil - g.time_mil_old) / 1000.0f);
        g.time_mil_old = time_mil;

        // clear the window, call the scene.render routine, and swap buffers.
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
        g.scene.render();
        glutSwapBuffers();
    };
    glutDisplayFunc(cb_render);
    glutIdleFunc(cb_render);

    g.time_mil_old = glutGet(GLUT_ELAPSED_TIME);
    g.scene.init();

    glutMainLoop();

    // if we get here, something odd has happened.
    return 1;
}
