#include "grid_square.h"
#include "evdev.h"
#include <iostream>
#include <string>
#include <list>
#include <unistd.h>

#define CLS (("\033[2J\033[1;1H"))

int main(void) {

    std::string grid_type_request;
    std::getline(std::cin, grid_type_request, ',');
    auto history = std::list<const maze::GridBase*>();

    if (grid_type_request.compare("square") == 0) {
        size_t w,h;
        char discard; // ignore commas
        std::cin >> w >> discard >> h >> discard;

        auto grid = new maze::GridSquare(w, h);
    
        std::cin >> grid->ingest_block();

        history.push_back(grid);
    }
    else {
        std::cerr << "Requested an unknown grid type: \"" << grid_type_request << "\"." << std::endl;
        exit(1);
    }

    while(std::cin) {
        auto grid = new maze::GridSquare(*(maze::GridSquare*)history.back());

        std::cin >> grid->ingest_stream();
        
        if (grid != history.back()) {
            history.push_back(grid);
        }
    }

//  Evdev dev = Evdev("/dev/input/by-id/usb-wilba.tech_wilba.tech_WT60-D_0-event-kbd");

    size_t i = 1;
    for (auto it = history.begin() ; it != history.end() ; ++it) {
        usleep(100000);
//      dev.process_events();
        std::cout << CLS << (*it)->ascii() << std::endl << "(" << i << "/" << history.size() << ")" << std::endl;
        ++i;
    }


    return 0;
}
