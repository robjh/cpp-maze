#include "gen_prims.h"
#include <stdlib.h>

namespace maze {

    GenPrims::GenPrims(GridBase& grid) : GenBase(grid) { }

    void GenPrims::initalise(void) {
        // choose a random cell.
        // mark it as in the maze.
        // add its neighbours to the consideration-list.
        unsigned int neighbour_count;
        size_t first = rand() % m_grid.length();
        m_grid.current(first);
        m_grid.visit(first);
        uint8_t neighbours = m_grid.unvisited_neighbours(first, &neighbour_count, false);
        for (size_t i = 0 ; i < neighbour_count ; ++i) {
            uint8_t n_dir = nth_high_bit(neighbours, i);
            size_t n_index = m_grid.go(first, n_dir);
            m_grid.tag(n_index);
            m_set.insert(n_index);
        }
    }

    void GenPrims::step(void) {
        // choose a random cell from the consideration list.
        // break down a random wall into the maze, mark it as in the maze.
        // add its neighbours to the consideration-list

        size_t current = [&s = this->m_set]()->size_t{
            auto it = s.cbegin();
            int random = rand() % s.size();
            std::advance(it, random);

            return *it;
        }();
        m_set.erase(current);
        m_grid.tag_clear(current);
        m_grid.current(current);
        m_grid.visit(current);
        unsigned int neighbour_count;
        uint8_t visited_neighbours = m_grid.visited_neighbours(current, &neighbour_count, false);
        m_grid.break_wall(current, nth_high_bit(visited_neighbours, rand() % neighbour_count));

        uint8_t neighbours = m_grid.unvisited_neighbours(current, &neighbour_count, false);
        for (size_t i = 0 ; i < neighbour_count ; ++i) {
            uint8_t n_dir = nth_high_bit(neighbours, i);
            size_t n_index = m_grid.go(current, n_dir);
            m_grid.tag(n_index);
            m_set.insert(n_index);
        }

        if (m_set.empty()) {
            m_complete = true;
        }
    }

}
