#include "gen_dfs_imperfect.h"
#include "exception.h"
#include <stdlib.h>

#include <iostream>
namespace maze {

    GenDfsImperfect::GenDfsImperfect(GridBase& grid) : GenBase(grid) { }

    void GenDfsImperfect::initalise(void) {
        const size_t last_cell = m_grid.length() - 1;
        m_grid.current(last_cell);
        m_grid.visit(last_cell);
        m_grid.tag(last_cell);
        m_stack.push_back(last_cell);
    }

    void GenDfsImperfect::step(void) {
        // if there are unvisited neighbours,
        //   choose one at random,
        //   break the wall,
        //   visit and make it current,
        //   add it to the stack.
        // else, pop from the stack and make the new head current.
        //   if the stack is empty, the algorithm is complete.

        const size_t cur = m_stack.back();
        unsigned int candidate_count;
        uint8_t candidate_neighbours = m_grid.unvisited_neighbours(cur, &candidate_count, false);

        if (candidate_count > 0) {
            uint8_t next_direction = nth_high_bit(candidate_neighbours, rand() % candidate_count);
            const size_t next = m_grid.go(cur, next_direction);

            m_grid.break_wall(cur, next_direction);
            m_grid.visit(next);
            m_grid.tag(next);
            m_grid.current(next);
            m_stack.push_back(next);
            m_backtracking = false;
        }

        else {
            if (m_backtracking == false) {
                // we're beginning to backtrack, break a random wall
                unsigned int wall_count = 0;
                uint8_t walls = m_grid.walls(cur);
                for (unsigned int i = 0 ; i < m_grid.wall_count() ; ++i) {
                    if (walls & (0x01 << i)) ++wall_count;
                }
                while (wall_count > 0) {
                    uint8_t dir = nth_high_bit(walls, rand() % wall_count);
                    try {
                        m_grid.go(cur, dir); // will throw if we cant go this way
                        m_grid.break_wall(cur, dir);
                        break;

                    } catch (E_OutOfBounds &e) {
                        walls &= ~dir;
                        --wall_count;
                    }
                }
            }
            m_backtracking = true;
            m_grid.tag_clear(cur);
            m_stack.pop_back();
            m_grid.current(m_stack.back());
        }

        if (m_stack.empty()) {
            m_complete = true;
        }

    }

}
