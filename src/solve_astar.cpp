#include "solve_astar.h"
#include "exception.h"

#include <iostream>

namespace maze {

    SolveAstar::SolveAstar(GridBase& grid) : SolveBase(grid) { }

    void SolveAstar::initalise(size_t from) {
        int g = 0;
        int f = m_grid.huristic(from, m_grid.goal_get());
        m_open.push_back({from, m_grid.length(), f, g});
        m_grid.value(from, f);
        m_grid.tag(from);
        m_grid.current(from);
        m_path_found = false;
    }

    void SolveAstar::step(void) {
        // if the open set is empty, the search has failed.
        // choose the cell from the open set with the best `f` value
        //   where `f = g + h`
        //   and `g = steps from the start node`
        //   and `h = huristic distance to the goal`
        // mark the current cell as visited.
        // if the current cell is the goal, success.
        // for each unvisited neighbour;
        //   calculate new g and f values.

        if (m_path_found) {
            if (m_path.empty()) {
                m_complete = true;
                return;
            }
            m_grid.current(m_path.back());
            m_path.pop_back();
            return;
        }

        if (m_open.empty()) {
            throw E_GoalNotFound();
        }

        auto cur = lowest_f();
        remove(cur.index);
        m_grid.tag_clear(cur.index);
        m_grid.visit(cur.index);

        if (m_grid.goal(cur.index)) {
            m_complete = true;
            construct_path(cur);
        }

        m_close.push_back(cur);

        unsigned int neighbours_count;
        uint8_t neighbours = m_grid.unvisited_neighbours(cur.index, &neighbours_count, true);
        for (unsigned int i = 0 ; i < neighbours_count ; ++i) {
            size_t n = m_grid.go(cur.index, nth_high_bit(neighbours, i));
            int g = cur.g + 1;
            int f = g + m_grid.huristic(n, m_grid.goal_get());
            Cell newcell = {n, cur.index, f, g};
            if (m_grid.goal(n)) {
                construct_path(newcell);
                return;
            }
            Cell * cellp;
            if (in_open(n, &cellp)) {
                // update cellp if it's new g value is smaller.
                //   I dont think this will ever happen because all cells have a travel cost of "1",
                //   but im leaving this as-is incase that changes in the future.
                if (cellp->g < g) {
                    cellp->g = g;
                    cellp->parent = cur.index;
                }
            }
            else {
                m_grid.value(n, f);
                m_open.push_back(newcell);
                m_grid.tag(n);
            }

        }

    }

    bool SolveAstar::in_open(size_t index, Cell** cellpp) {
        for (auto it = m_open.begin() ; it != m_open.end() ; ++it) {
            if (it->index == index) {
                if (cellpp != nullptr) *cellpp = &(*it);
                return true;
            }
        }
        return false;
    }
 
    SolveAstar::Cell SolveAstar::lowest_f() {
        Cell ret;
        ret.f = INT32_MAX;
        for (auto it = m_open.begin() ; it != m_open.end() ; ++it) {
            if (it->f < ret.f) ret = *it;
        }
        return ret;
    }
    void SolveAstar::remove(size_t index) {
        for (auto it = m_open.begin() ; it != m_open.end() ; ++it) {
            if (it->index == index) {
                m_open.remove(*it);
                return;
            }
        }
        throw E_UnexpectedEof();
    }
    void SolveAstar::construct_path(Cell& goal) {
        Cell cur = goal;
        m_grid.tag_clear_all();
        while (cur.parent < m_grid.length()) {
            m_path.push_back(cur.index);
            bool found = false;
            for (auto it = m_close.begin() ; it != m_close.end() ; ++it) {
                if (it->index == cur.parent) {
                    found = true;
                    cur = *it;
                    break;
                }
            }
            if (!found) {
                std::cerr << "didnt find a parent in the close list." << std::endl;
                throw E_GoalNotFound();
            }
            m_grid.tag(cur.index);
        }
        m_path_found = true;
    }

}
