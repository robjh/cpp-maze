#include "evdev.h"
#include <sstream>
#include <string>
#include <iostream>

extern "C"{
#include <fcntl.h>
#include <libevdev/libevdev.h>
#include <string.h>
#include <errno.h>
}


Evdev::E_EvdevGeneric::E_EvdevGeneric() : m_err("A libevdev error occured.") {}
const char * Evdev::E_EvdevGeneric::what () const throw () {
    return m_err.c_str();
}
Evdev::E_InitEvdev::E_InitEvdev(int error_num)  {
    std::stringstream errmsg(m_err);
    errmsg << "Failed to init libevdev (" << strerror(error_num) << ").";
    m_err = errmsg.str();
}


Evdev::Evdev(const char * device) : m_dev(nullptr) {
    int retcode = 1;

    m_fd = open(device, O_RDONLY|O_NONBLOCK);
    retcode = libevdev_new_from_fd(m_fd, &m_dev);
    if (retcode < 0) {
        throw E_InitEvdev(-retcode);
    }

    std::cout << "Input device name: \"" << libevdev_get_name(m_dev) << "\"" << std::endl << std::hex
              << "Input device ID: bus 0x" << libevdev_get_id_bustype(m_dev)
              <<              " vendor 0x" << libevdev_get_id_vendor(m_dev)
              <<             " product 0x" << libevdev_get_id_product(m_dev) << std::endl;

}

void Evdev::process_events(void) {
    int retcode = -1;
    do {
            struct input_event ev;
            retcode = libevdev_next_event(m_dev, LIBEVDEV_READ_FLAG_NORMAL, &ev);
            if (retcode == 0)
                    printf("Event: %s %s %d\n",
                           libevdev_event_type_get_name(ev.type),
                           libevdev_event_code_get_name(ev.type, ev.code),
                           ev.value);
    } while (retcode == 1 || retcode == 0 || retcode == -EAGAIN);
}

/*
void evtest2(void) {
    struct libevdev *dev = NULL;
    int rc = 1;
    int fd;

    fd = open("/dev/input/event0", O_RDONLY|O_NONBLOCK);
    rc = libevdev_new_from_fd(fd, &dev);
    if (rc < 0) {
          //  fprintf(stderr, "Failed to init libevdev (%s)\n", strerror(-rc));
            exit(1);
    }
    printf("Input device name: \"%s\"\n", libevdev_get_name(dev));
    printf("Input device ID: bus %#x vendor %#x product %#x\n",
           libevdev_get_id_bustype(dev),
           libevdev_get_id_vendor(dev),
           libevdev_get_id_product(dev));
    if (!libevdev_has_event_type(dev, EV_REL) ||
        !libevdev_has_event_code(dev, EV_KEY, BTN_LEFT)) {
            printf("This device does not look like a mouse\n");
            exit(1);
    }

    do {
            struct input_event ev;
            rc = libevdev_next_event(dev, LIBEVDEV_READ_FLAG_NORMAL, &ev);
            if (rc == 0)
                    printf("Event: %s %s %d\n",
                           libevdev_event_type_get_name(ev.type),
                           libevdev_event_code_get_name(ev.type, ev.code),
                           ev.value);
    } while (rc == 1 || rc == 0 || rc == -EAGAIN);
}
//*/
