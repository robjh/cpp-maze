#include "gen_base.h"
#include "exception.h"

namespace maze {

    GenBase::GenBase(GridBase& grid) : m_grid(grid), m_complete(false) { }
    GenBase::~GenBase(void) { }

    uint8_t GenBase::nth_high_bit(uint8_t flags, unsigned int bitcount) const {
        for (int i = 0 ; i < 8 ; ++i) {
            if ((0x01 << i & flags) > 0) {
                if (bitcount-- == 0) {
                    return 0x01 << i;
                }
            }
        }
        throw E_NoMoreBits();
    }

    bool GenBase::complete(void) {
        return m_complete;
    }

    void GenBase::generate(void) {
        initalise();
        while (!m_complete) {
            step();
        }
        m_grid.clean();
    }

}
