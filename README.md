## building

requires meson and ninja, as well as the following libraries;
- evdev
- gl
- glu
- glut
- glew
- glm

```
meson builddir
ninja -C builddir/

# now you can use the binaries to generate/solve mazes.
cd builddir
./generator -a dfs 10 10 | ./solver -a dijkstra | ./renderer_ascii
./generator -a prims 10 10 | ./solver -a astar | ./renderer_gl
```
